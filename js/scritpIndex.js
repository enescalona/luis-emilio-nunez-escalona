
$(function(){
    $("[data-toogle='tooltip']").tooltip();
    $("[data-toogle='popover']").popover();
    
    $('.carousel').carousel({
        interval: 2000
    });

    $('#pernikCont').on('show.bs.modal', function(e){
    console.log('Mostrando modal');

        $('#pernikCont').removeClass('btn-outline-success');
        $('#pernikCont').addClass('btn-outline-primary');
        $('#pernikCont').prop('disabled', true);

    });

    $('#pernikCont').on('shown.bs.modal', function(e){
        console.log('Modal mostrado');
    });

    $('#pernikCont').on('hide.bs.modal', function(e){
        console.log('Ocultando modal');
    });

    $('#pernikCont').on('hidden.bs.modal', function(e){
        console.log('Modal Ocultado');
        
        $('#pernikCont').prop('disabled', false);
        $('#pernikCont').removeClass('btn-outline-primary');
        $('#pernikCont').addClass('btn-outline-success');

    });
    
});
